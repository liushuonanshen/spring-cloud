package com.lx.vo;

import java.io.Serializable;

public class GenderCount implements Serializable {
    private Integer value;
    private String name;

    @Override
    public String toString() {
        return "GenderCount{" +
                "value=" + value +
                ", name='" + name + '\'' +
                '}';
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if("0".equals(name) || "女".equals(name)){
            this.name = "女";
        }else{
            this.name = "男";
        }
    }

    public GenderCount(Integer value, String name) {
        this.value = value;
        this.name = name;
    }

    public GenderCount() {
    }
}
