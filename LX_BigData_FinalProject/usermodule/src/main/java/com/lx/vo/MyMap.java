package com.lx.vo;

import java.util.HashMap;
import java.util.Map;

public class MyMap extends HashMap {
    /**
     * 直接使用无参构造创建的是正常情况
     */
    public MyMap(){
        put("code",0);
        put("status","success");
    }
    /**
     * @param code 错误码
     * @param message 错误信息
     * @return 返回错误信息及错误码
     */
    public static MyMap error(Integer code,String message){
        MyMap map = new MyMap();
        map.put("status","fail");
        map.put("code",code);
        map.put("msg",message);
        return map;
    }
    /**
     * @param msg 错误信息
     * @return 返回错误码为500的错误信息
     */
    public static MyMap error(String msg) {
        return error(500, msg);
    }

    /**
     * @return  返回未知的错误信息
     */
    public static MyMap error(){
        return error(500,"未知异常，请联系管理员");
    }

    /**
     * @return  返回不携带任何信息的正确map
     */
    public static MyMap ok(){
        return new MyMap();
    }

    public static MyMap ok(Map<String,Object> map){
        MyMap myMap = new MyMap();
        myMap.putAll(map);
        return myMap;
    }
    /**
     * @param msg
     * @return 返回带正确提示信息的map
     */
    public static MyMap ok(String msg){
        MyMap map = new MyMap();
        map.put("msg",msg);
        map.put("status","success");
        return map;
    }
    //重写父类put方法
    public MyMap put(String key ,Object value){
        super.put(key,value);
        return this ;
    }
}
