package com.lx.controller;

import com.lx.bean.Menu;
import com.lx.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("menu")
public class MenuController {
    @Autowired
    private MenuService menuService;

    @RequestMapping("selectAllMenu")
    public List<Menu> selectAllMenu(){
        List<Menu> menus = menuService.selectAll();
        return menus;
    }
}
