package com.lx.mapper;

import com.lx.bean.Menu;

import java.util.List;

public interface MenuMapper {

    List<Menu> queryAllMenu();
}

