package com.lx.common.cache;

import com.lx.common.config.MyApplicationContextAware;
import org.apache.ibatis.cache.Cache;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;

/**
 * 自定义缓存
 * 把数据缓存到redis中，需要有RedisTemplate对象
 *
 * 在使用redis的时候，应该使用hash类型存储数据
 * 代码中id(namespace)作为redis中的key
 *
 * 代码中key作为redis中的field:本质上把数据查出来存数据的id(唯一主键)值
 *
 * 代码中value作为redis的value
 *
 */
public class MyCache implements Cache {

//    @Autowired
//    private RedisTemplate redisTemplate;

    //RedisTemplate redisTemplate = (RedisTemplate) MyApplicationContextAware.getBean("redisTemplate");
    //上面这一条创建对象的代码有可能报空指针异常，因为Mybatis框架跟Spring框架创建对象是两个线程，如果mybatis先创建该对象，但调用了spring里面的方法就报空指针
    private final String id; //namespace

    public MyCache(String id) {
        this.id = id;
    }

    //id 唯一标识一个命名空间
    @Override
    public String getId() {
        return id;
    }

    //向缓存中写入数据
    @Override
    public void putObject(Object key, Object value) {
        RedisTemplate redisTemplate = (RedisTemplate) MyApplicationContextAware.getBean("redisTemplate");
        HashOperations hashOperations = redisTemplate.opsForHash();
        /*
             key = -811575049:2649946373:com.lx.mapper.UserMapper.queryUserByPage:0:2147483647:select * from t_user limit ?,?:0:10:SqlSessionFactoryBean
             value = [User{id=1, name='李想', sex=1, password='123', birthDay=Mon Dec 07 00:00:00 CST 2020, photo='照骗', email='1015496993@qq.com'}, ...]
         */
        //System.out.println("value = " + value);
        hashOperations.put(id,key.toString(),value);
        redisTemplate.expire(id,10, TimeUnit.MINUTES);//设置键的十分钟过期时间
    }

    //根据 key 从缓存中获取数据
    @Override
    public Object getObject(Object key) {
        RedisTemplate redisTemplate = (RedisTemplate) MyApplicationContextAware.getBean("redisTemplate");
        HashOperations hashOperations = redisTemplate.opsForHash();
        Object o = hashOperations.get(id, key.toString());//根据大key和小key获取值
        return o;
    }

    //根据key从缓存中移除数据
    @Override
    public Object removeObject(Object key) {
        RedisTemplate redisTemplate = (RedisTemplate) MyApplicationContextAware.getBean("redisTemplate");
        HashOperations hashOperations = redisTemplate.opsForHash();
        return hashOperations.delete(id,key.toString());
    }

    //清空缓存
    @Override
    public void clear() {
        new HashMap(){
            {put("a","b");}
        };
        RedisTemplate redisTemplate = (RedisTemplate) MyApplicationContextAware.getBean("redisTemplate");
        redisTemplate.delete(id);
    }

    //获取缓存长度(根据key的数量)
    @Override
    public int getSize() {
        RedisTemplate redisTemplate = (RedisTemplate) MyApplicationContextAware.getBean("redisTemplate");
        HashOperations hashOperations = redisTemplate.opsForHash();
        Long size = hashOperations.size(id);
        int i = size.intValue();
        return i;
    }

    //读写锁
    @Override
    public ReadWriteLock getReadWriteLock() {
        return null;
    }
}
