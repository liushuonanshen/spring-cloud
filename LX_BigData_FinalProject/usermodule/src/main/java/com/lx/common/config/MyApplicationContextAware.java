package com.lx.common.config;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

/**
 * ApplicationContextAware会自动携带applicationContext对象，
 * 通过setApplicationContext方法给类中的属性赋值
 */
@Configuration
public class MyApplicationContextAware implements ApplicationContextAware {

    private static ApplicationContext applicationContext;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext=applicationContext;
    }

    //到工厂中根据名字获取到一个对象
    public static Object getBean(String beanName){
        return applicationContext.getBean(beanName);
    }
}