package com.lx.service.impl;

import com.lx.bean.Menu;
import com.lx.bean.User;
import com.lx.mapper.MenuMapper;
import com.lx.mapper.UserMapper;
import com.lx.service.MenuService;
import com.lx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> selectAll() {
        List<Menu> menus = menuMapper.queryAllMenu();
        return menus;
    }
}
