package com.lx.service;

import com.lx.bean.Menu;

import java.util.List;

public interface MenuService {
    List<Menu> selectAll();
}
