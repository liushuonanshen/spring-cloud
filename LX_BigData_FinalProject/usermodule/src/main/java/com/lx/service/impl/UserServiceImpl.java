package com.lx.service.impl;

import com.lx.bean.User;
import com.lx.mapper.UserMapper;
import com.lx.service.UserService;
import com.lx.vo.GenderCount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User addUser(User user) {
        userMapper.addUser(user);
        return user;
    }

    @Override
    public User queryUserByNameAndPassword(User user) {
        User user1 = userMapper.queryUserByNameAndPassword(user);
        return user1;
    }

    @Override
    public List<User> queryUserByPage(Integer pageIndex, Integer pageSize, String column, Object value) {
        Integer offset = (pageIndex-1)*pageSize;
        List<User> users = userMapper.queryUserByPage(offset, pageSize, column, value);
        return users;
    }

    @Override
    public int queryUserCount(String column, Object value) {
        int i = userMapper.queryCount(column, value);
        return i;
    }

    @Override
    public User queryUserById(Integer id) {
        User user = userMapper.queryUserById(id);
        return user;
    }

    @Override
    public void deleteByUserIds(Integer[] ids) {
        userMapper.deleteByUserIds(ids);
    }

    @Override
    public void deleteUserById(Integer id) {
        userMapper.deleteUserById(id);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    @Override
    public List<GenderCount> countGender() {
        List<GenderCount> genderCounts = userMapper.countGender();
        return genderCounts;
    }
}
