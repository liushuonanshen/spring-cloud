package com.lx.bean;

import java.io.Serializable;
import java.util.List;

public class Menu implements Serializable {
    /*
    id int primary key auto_increment,
	menu_name varchar(32),
	href varchar(100),
	parent_id	int
     */
    private Integer id;
    private String menuName;
    private String href;
    private List<Menu> secondMenu;

    public Menu() {
    }

    public Menu(Integer id, String menuName, String href, List<Menu> secondMenu) {
        this.id = id;
        this.menuName = menuName;
        this.href = href;
        this.secondMenu = secondMenu;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<Menu> getSecondMenu() {
        return secondMenu;
    }

    public void setSecondMenu(List<Menu> secondMenu) {
        this.secondMenu = secondMenu;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "id=" + id +
                ", menuName='" + menuName + '\'' +
                ", href='" + href + '\'' +
                ", secondMenu=" + secondMenu +
                '}';
    }
}
