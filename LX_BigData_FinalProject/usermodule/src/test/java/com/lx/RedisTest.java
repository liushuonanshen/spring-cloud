package com.lx;

import org.aspectj.weaver.ast.Var;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.security.Key;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {UsermoduleApplication.class})
public class RedisTest {
    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void getRedisTemplateTest(){
        System.out.println("redisTemplate = " + redisTemplate);
    }

    @Test
    public void putTest(){
        HashOperations hashOperations = redisTemplate.opsForHash();
        hashOperations.put("key","hashkey1","value1");
    }

    @Test
    public void getTest(){
        HashOperations hashOperations = redisTemplate.opsForHash();
        Object o = hashOperations.get("key","hashkey1");
        System.err.println(o);
    }
    @Test
    public void flushTest(){
        redisTemplate.delete("key");
    }

    @Test
    public void deleteTest(){
        HashOperations hashOperations = redisTemplate.opsForHash();
        Long delete = hashOperations.delete("key", "hashkey1");
        System.out.println("delete = " + delete);
    }

    @Test
    public void sizeTest(){
        HashOperations hashOperations = redisTemplate.opsForHash();
        Long key = hashOperations.size("key");
        System.out.println("key = " + key);
    }
}
