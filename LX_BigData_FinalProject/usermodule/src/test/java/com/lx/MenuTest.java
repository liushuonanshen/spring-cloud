package com.lx;

import com.lx.bean.Menu;
import com.lx.bean.User;
import com.lx.mapper.UserMapper;
import com.lx.service.MenuService;
import com.lx.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {UsermoduleApplication.class})
public class MenuTest {
    @Autowired
    private MenuService menuService;

    @Test
    public void selectAllMenuTest(){
        List<Menu> menus = menuService.selectAll();
        for (Menu menu : menus) {
            System.out.println("menu = " + menu);
        }
    }
}
