package com.lx;

import com.lx.bean.User;
import com.lx.mapper.UserMapper;
import com.lx.service.UserService;
import com.lx.vo.GenderCount;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {UsermoduleApplication.class})
public class UserTest {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;

    @Test
    public void addUserTest(){
        User user = new User(0, "lb", "123", 1, "photo", new Date(), "email");
        userService.addUser(user);
    }

    @Test
    public void selectUserByNameAndPasswordTest(){
        User user = new User();
        user.setName("lx");
        user.setPassword("123");
        User result = userService.queryUserByNameAndPassword(user);
        System.out.println("result = " + result);
    }

    @Test
    public void queryUserByIdTest(){
        User user = userService.queryUserById(9);
        System.out.println("user = " + user);
    }

    @Test
    public void deleteUserByIdTest(){
        userService.deleteUserById(9);
    }

    @Test
    public void deleteByUserIdsTest(){
        Integer [] ids = {7,8};
//        userService.deleteByUserIds(ids);
    }

    @Test
    public void queryUserByPageTest(){
        List<User> users = userService.queryUserByPage(2, 5, null, null);
        for (User user : users) {
            System.out.println("user = " + user);
        }
    }

    @Test
    public void queryCountTest(){
        int i = userService.queryUserCount(null, null);
        System.out.println("i = " + i);
    }

    @Test
    public void updateTest(){
        User user = new User(1, "李想", "123", 1, "照骗", new Date(), "1015496993@qq.com");
        userService.updateUser(user);
    }

    @Test
    public void pageTest(){
        List<User> users = userService.queryUserByPage(2, 5, "photo", "photo");
        int i = userService.queryUserCount("photo", "photo");
        for (User user : users) {
            System.out.println("user = " + user);
        }
        System.out.println("i = " + i);
    }
    @Test
    public void genderCountTest(){
        List<GenderCount> genderCounts = userService.countGender();
        for (GenderCount genderCount : genderCounts) {
            System.err.println(genderCount);
        }
    }
}
