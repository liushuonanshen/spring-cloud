$.fn.extend({
    inputFeatures:function () {
        //获取表单的所有input标签
        var inputs=$(this).find("input");
        //为所有的Input标签绑定两个事件
        var inputTimes = {};//定义一个数组记录每个输入框的输入时间
        $.each(inputs,function (i,item) {//i->input的下标，item->input对象
            //记录第一次键盘按下的时间
            $(item).bind("keydown",function () {
                var start=  $(this).data("start")//根据start这个key到这个input输入框中获取数据
                // console.log(start);
                if(!start){//如果是第一次按下
                    var now = new Date().getTime();//时间戳
                    // console.log(start+"  第  "+i+"   个input开始时间")
                    //把时间戳放入到这个input输入框中，名字是start
                    $(this).data("start",now)
                }
            })

            //记录最后一次键盘弹起的时间
            $(item).bind("keyup",function () {
                var start=  $(this).data("start");//获取到上一步存放的时间
                var end=  new Date().getTime();//获取到当前键盘按键弹起的时间，每次弹起都会计算
                // inputTimes[i]=end-start;
                //当用户输入错误并没有删完的时候累计时间  删除完成重新输入时间归0
                if( $(this).val()==""){
                    //如果输入框中没有内容，把start设置为null
                    $(this).data("start",null)
                    // inputTimes[i]= 0
                }else {
                    inputTimes[i] = end - start//在数组中存放当前input的输入时间
                }
                var inputFeature="";
                for(var k in inputTimes){
                    inputFeature+=inputTimes[k]+","
                }
                console.log(inputFeature)
                //放入到cookie中
                $.cookie('feature', inputFeature);
            })

        })

    }
})