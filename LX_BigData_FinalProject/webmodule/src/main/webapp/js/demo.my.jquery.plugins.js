$.fn.extend({
    pluginName:function (param) {
        console.log("my first jquery plugin" + param);
    }
    ,
    //插件名:function(param){实现功能}
    //刷新验证码
    flushCode:function (option) {
        //默认url；如果在使用 flushCode 的时候，没有传达过来请求验证码的地址，就使用这个地址
        var defaultOption={
            url:"user/code"
        }
        //把option合并到defaultOption
        option=$.extend(defaultOption,option)

        //把option中的url属性值获获取到之后，放入到img标签的src属性上
        $(this).prop("src",option.url)

        //当点击验证码图片的时候，让img标签的src属性值变化(在地址后面拼接一个动态参数)
        $(this).click(function(){
            $(this).prop("src",option.url+"?t="+new Date().getTime())
        })
    }
    ,
    showMessage:function (param) {
        var defaultOption={
            url:"user/getNowUser"
        }
        param=$.extend(defaultOption,param)
        var $this=this;
        // jQuery.get(param.url,function(data){
        //     $($this).html(data+",欢迎您！")
        // });

        $.ajax({
            url:param.url,
            success:function (data) {
                $($this).html(data.username+",欢迎您！")
            }
        })
    }
    ,
    showNowTime:function () {
        var weekDay=["星期日","星期一","星期二","星期三","星期四","星期五","星期六"]
        var $this = this
        setInterval(function(){
            var date = new Date();
            var yyyy=date.getFullYear();
            var MM = date.getMonth()+1;
            var dd = date.getDate();
            var HH = date.getHours();
            var mm = date.getMinutes();
            var ss = date.getSeconds();
            var week = date.getDay();
            var str = yyyy+"年"+MM+"月"+dd+"日 "+HH+":"+mm+":"+ss+" "+weekDay[week];
            $($this).html(str)
        },1000);
    }
})