package com.lx.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@Component
public class InputFeatureInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String interFeature = "";
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if("feature".equals(cookie.getName())){
                interFeature=cookie.getValue();
            }
        }
        System.out.println(Arrays.toString(interFeature.split("%2C")));

        return true;
    }
}
