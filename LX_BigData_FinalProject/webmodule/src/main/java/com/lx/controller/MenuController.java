package com.lx.controller;

import com.lx.feign.UserFeign;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("menu")
@RefreshScope
public class MenuController {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UserFeign userFeign;

    @HystrixCommand
    @GetMapping("selectAllMenu")
    public Object selectAllMenu(){
        Object o = userFeign.selectAllMenu();//使用过feign替换原始的restTemplate方法
//        Object result = restTemplate.getForObject("http://usermodule/menu/selectAllMenu", Object.class);
        return o;
    }

    /*
    下面方法用来测试 BUS 总线
     */
    @Value("${aaa}")
    private String aaa;

    @RequestMapping("test")
    public String testConfig(){
        System.out.println("MenuController.testConfig");
        return aaa;
    }
}
