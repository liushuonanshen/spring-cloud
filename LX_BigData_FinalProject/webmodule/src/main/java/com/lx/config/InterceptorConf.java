package com.lx.config;

import com.lx.interceptor.InputFeatureInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class InterceptorConf implements WebMvcConfigurer {

    @Autowired
    private InputFeatureInterceptor inputFeatureInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(inputFeatureInterceptor)
        .addPathPatterns("/user/login");  //要拦截的请求
        //.excludePathPatterns();//要放行的请求
    }

}
