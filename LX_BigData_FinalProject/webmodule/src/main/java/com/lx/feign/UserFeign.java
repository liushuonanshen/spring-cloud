package com.lx.feign;

import com.lx.bean.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @FeignClient 标记该接口为一个代替restTemplate发请求的接口
 * 其中 name 属性在整个项目中应该唯一
 * configuration 属性用来指定日志级别
 */
@FeignClient(name = "usermodule",configuration = {FeignLog.class})
public interface UserFeign {

    //查询用户模块中的菜单    应该分开的
    @RequestMapping(value = "/menu/selectAllMenu")
    Object selectAllMenu();

    //-------------以下是用户模块------------
    @PostMapping("/user/registerUser")
    Map registerUser(@RequestBody User user);

    @RequestMapping(value = "/user/userLogin",method = RequestMethod.POST)
    Map userLogin(@RequestBody User user);

    @RequestMapping(value = "/user/updateUser",method = RequestMethod.PUT)
    Map updateUser(@RequestBody User user);

    @DeleteMapping("/user/deleteUserByIds")
    Map deleteUserByIds(@RequestBody  Integer[] ids);

    @DeleteMapping("/user/deleteUserById/{id}")
    Map deleteUserById(@PathVariable("id") Integer id);

    @RequestMapping("/user/queryUserByPage")
    List<User> queryUserByPage(@RequestParam(value = "pageIndex",defaultValue = "1") Integer pageIndex,
                                      @RequestParam(value = "rows",defaultValue = "10") Integer rows,
                                      @RequestParam(value = "column",required = false) String column,
                                      @RequestParam(value = "value",required = false) String value);

    @RequestMapping("/user/queryUserCount")
    Integer queryUserCount(@RequestParam("column")String column,@RequestParam("value") String value);

    @RequestMapping("/user/queryUserById")
    User queryUserById(Integer id);

    @RequestMapping("/user/genderCount")
    List<Map> genderCount();
}
